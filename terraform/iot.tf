# aws_iot_certificate cert

# aws_iot_policy pub-sub

# aws_iot_policy_attachment attachment

# aws_iot_thing temp_sensor

# aws_iot_thing_principal_attachment thing_attachment

# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py

# aws_iot_topic_rule rule for sending invalid data to DynamoDB

# aws_iot_topic_rule rule for sending valid data to Timestream


###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}


resource "aws_iot_certificate" "cert" {
  active = true
}

resource "aws_iot_policy" "pub-sub" {
  name = "PubSubToAnyTopic"
  policy = file("${path.module}/files/iot_policy.json")
}

resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pub-sub.name
  target = aws_iot_certificate.cert.arn
}

resource "aws_iot_thing" "temp_sensor1_zone_1" {
  name = "temp_sensor1_zone_1"
}

resource "aws_iot_thing_principal_attachment" "thing_attachment1" {
  principal = aws_iot_certificate.cert.arn
  thing = aws_iot_thing.temp_sensor1_zone_1.name
}

resource "aws_iot_thing" "temp_sensor2_zone_1" {
  name = "temp_sensor2_zone_1"
}

resource "aws_iot_thing_principal_attachment" "thing_attachment_2" {
  principal = aws_iot_certificate.cert.arn
  thing = aws_iot_thing.temp_sensor2_zone_1.name
}

resource "aws_iot_thing" "temp_sensor1_zone_2" {
  name = "temp_sensor1_zone_2"
}

resource "aws_iot_thing_principal_attachment" "thing_attachment_3" {
  principal = aws_iot_certificate.cert.arn
  thing = aws_iot_thing.temp_sensor1_zone_2.name
}

resource "aws_iot_thing" "temp_sensor2_zone_2" {
  name = "temp_sensor2_zone_2"
}

resource "aws_iot_thing_principal_attachment" "thing_attachment_4" {
  principal = aws_iot_certificate.cert.arn
  thing = aws_iot_thing.temp_sensor2_zone_2.name
}

data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}

resource "aws_iot_topic_rule" "temperature_rule" {
  name = "TemperatureRule"
  description = "Rule to insert messages into DynamoDB"
  enabled = true
  sql = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.temperature-table.name
    }
  }
}

resource "aws_iot_topic_rule" "temperature_rule_2" {
  name = "TemperatureRule2"
  description = "Rule to insert messages into DynamoDB"
  enabled = true
  sql = "SELECT *  FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

  timestream {
    database_name = aws_timestreamwrite_database.example.database_name
    table_name    = aws_timestreamwrite_table.example_table.table_name
    role_arn = aws_iam_role.iot_role.arn
    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
  }
}


/*resource "aws_iot_topic_rule" "temperature_rule" {
  name = "TemperatureRule"
  description = "Rule to insert messages into DynamoDB"
  enabled = true
  sql = "SELECT *, timestamp() as timestamp FROM 'test/topic'"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.temperature-table.name
    }
  }
}

resource "aws_iot_topic_rule" "temperature_rule_iot_event" {
  name = "TemperatureRuleThreshold"
  description = "Rule to create a s3 file when the temperature exceed 35°C"
  enabled = true
  sql = "SELECT *, GETDATE() as date FROM 'test/topic' where temperature >= 35"
  sql_version = "2016-03-23"

  s3 {
    bucket_name = aws_s3_bucket.iot_bucket.bucket
    key = "test-topic/$${timestamp()}.txt"
    role_arn = aws_iam_role.iot_role.arn
  }
}

/*resource "aws_iot_logging_options" "logging_option" {
  default_log_level = "WARN"
  role_arn          = aws_iam_role.iot_role.arn
}*/